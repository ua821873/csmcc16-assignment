# encoding=utf-8

import logging
import re
import math
import pandas as pd


def readPsngerData(data_path):
    """
    read data from passenger data file
    :param data_path: the path of passenger data
    :return: passenger data, record format: [psnger_id, flt_id, frm_code, dst_code, dpt_time, ttl_time]
    """
    psnger_records = []
    records = set() # to store the valid record line
    with open(data_path, "r") as reader:
        line = reader.readline()
        line_num = 1
        dplt_num = 0
        nvld_num = 0
        while line:
            line = line.strip()
            line_num += 1

            if line in records:
                logging.info("line %d: '%s' is a duplicated record!" %(line_num, line))
                dplt_num += 1
            else:
                cols = line.split(",")
                if not isValidPsngRecord(cols):
                    logging.warning("line %d: '%s' is invalid!"%(line_num, line))
                    nvld_num += 1
                else:
                    psnger_records.append(cols)
                    records.add(line)
            line = reader.readline()
        logging.info("there are total %d duplicated records, and %d invalid records in data file '%s'"
                     % (dplt_num, nvld_num, data_path))
    return psnger_records


def isValid(ptn, s):
    """
    match the pattern 'ptn' with input string 's'
    :param ptn: the pattern
    :param s: the input string
    :return:
    """
    ptn = "\A" + ptn + "\Z"
    return re.match(ptn, s)


def isValidPsngRecord(record):
    """
    check the passenger record valid or not
    :param record: the input passenger record
    :return:
    """
    if len(record) != 6:
        return False

    psg_id_ptn = "[A-Z]{3}\d{4}[A-Z]{2}\d"
    flt_id_ptn = "[A-Z]{3}\d{4}[A-Z]"
    code_ptn = "[A-Z]{3}"
    dpt_tm_ptn = "\d{10}"
    flt_tm_ptn = "\d{1,4}"

    return isValid(ptn=psg_id_ptn, s=record[0]) and isValid(ptn=flt_id_ptn, s=record[1]) \
        and isValid(ptn=code_ptn, s=record[2]) and isValid(ptn=code_ptn, s=record[3]) \
        and isValid(ptn=dpt_tm_ptn, s=record[4]) and isValid(ptn=flt_tm_ptn, s=record[5])


def isValidFltRecord(record):
    """
    check the flight record valid or not
    :param record: the input flight record
    :return:
    """
    if len(record) != 4:
        return False

    name_ptn = "[A-Z]{3,20}"
    code_ptn = "[A-Z]{3}"
    pos_ptn = "-?\d+\.\d+"

    # return isValid(ptn=name_ptn, s=record[0]) and \
    return isValid(ptn=code_ptn, s=record[1]) \
        and isValid(ptn=pos_ptn, s=record[2]) and isValid(ptn=pos_ptn, s=record[3]) \
        and len(record[2]) <= 13 and len(record[3]) <= 13


def readPrtData(data_path):
    """
    read data from airport data file
    :param data_path: the path of airport data
    :return: airport data, record format: [name, code, latitude, longitude]
    """
    prt_records = []
    records = set() # to store the valid record line
    with open(data_path, "r") as reader:
        line = reader.readline()
        line_num = 0
        dplt_num = 0
        nvld_num = 0
        while line:
            line = line.strip()
            line_num += 1

            if line in records:
                logging.info("line %d: '%s' is a duplicated record!" % (line_num, line))
                dplt_num += 1
            else:
                cols = line.split(",")
                if not isValidFltRecord(cols):
                    logging.warning("line %d: '%s' is an invalid record!" %(line_num, line))
                    nvld_num += 1
                else:
                    prt_records.append(cols)
            line = reader.readline()
        logging.info("there are total %d duplicated records, and %d invalid records in data file '%s'"
                 % (dplt_num, nvld_num, data_path))
    return prt_records


def getDistance(lat1, lng1, lat2, lng2):
    """
    calculate the distance between position (lat1, lng1) and (lat2, lng2)
    :param lat1: the latitude of position 1
    :param lng1: the longitude of position 1
    :param lat2: the latitude of position 2
    :param lng2: the longitude of position 2
    :return:
    """
    rad = lambda d: d*math.pi/180.0
    EARTH_RADIUS = 6371
    radLat1=rad(lat1)
    radLat2=rad(lat2)
    a=radLat1-radLat2
    b=rad(lng1)-rad(lng2)

    s=2*math.asin(math.sqrt(math.pow(math.sin(a/2),2)+
    math.cos(radLat1)*math.cos(radLat2)*math.pow(math.sin(b/2),2)));

    s=s*EARTH_RADIUS;
    s=int(s*10000)/10000.0

    return s


def distance(lat1, lon1, lat2, lon2):
    """
    calculate the distance between position (lat1, lng1) and (lat2, lng2)
    :param lat1: the latitude of position 1
    :param lng1: the longitude of position 1
    :param lat2: the latitude of position 2
    :param lng2: the longitude of position 2
    :return:
    """
    theta = abs(lon1 - lon2)
    dist = math.sin(deg2rad(lat1)) * math.sin(deg2rad(lat2)) \
                + math.cos(deg2rad(lat1)) * math.cos(deg2rad(lat2)) \
                * math.cos(deg2rad(theta))
    dist = math.acos(dist)
    dist = rad2deg(dist)
    miles = dist * 60 * 1.1515*0.8684
    return miles


def deg2rad(degree):
    """
    convert angles to radians
    :param degree: the input angle
    :return:
    """
    return degree / 180 * math.pi


def rad2deg(radian):
    """
    convert radians to angles
    :param radian: the input radian
    :return:
    """
    return radian * 180 / math.pi


def writeFile(records, output_path, title):
    """
    write analysis result to file
    :param records: the analysis results with type 'list'
    :param output_path: the output path to store results
    :param title: the column names
    :return:
    """
    records_pd = pd.DataFrame(columns=title, data=records)
    records_pd.to_csv(output_path, encoding="utf-8")

