# encoding=utf-8

import logging
import sys
import os
from utils import readPrtData, readPsngerData, writeFile
from mapreduce import calcFltNum, calcFltInfo, calcPsngNumber, calcFltDists


def main():
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)  # get logger object
    logging.basicConfig(format="%(asctime)s: %(levelname)s: %(message)s")  # set log format
    logging.root.setLevel(level=logging.INFO)
    data_dir = "./data"
    out_dir = "./out"
    psnger_data_path = os.path.join(data_dir, "AComp_Passenger_data.csv")
    prt_data_path = os.path.join(data_dir, "Top30_airports_LatLong.csv")
    if not os.path.exists(psnger_data_path) or not os.path.exists(prt_data_path):
        logging.error("data files are not found!")
        exit(-1)
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    # read data
    psnger_records = readPsngerData(psnger_data_path)
    prt_records = readPrtData(prt_data_path)

    # calculate the number of flights from each airport
    prt_flt_info = calcFltNum(psnger_records=psnger_records, prt_records=prt_records)
    prt_flt_info_path = os.path.join(out_dir, "prt_flt_info.csv")
    prt_flt_title = ["airport_name", "airport_code", "latitude", "longitude", "flight_number"]
    writeFile(prt_flt_info, prt_flt_info_path, title=prt_flt_title)

    # create the list of flights based on the flight id
    flt_info = calcFltInfo(psnger_records=psnger_records)
    flt_info_path = os.path.join(out_dir, "flt_info.csv")
    flt_info_title = ["flight_id", "passenger_id", "from_airport_code", "to_airport_code", "departure_time",
                      "arrival_time", "flight_time"]
    writeFile(flt_info, flt_info_path, title=flt_info_title)

    # calculate the number of passengers on each flight
    flt_psnger_info = calcPsngNumber(psnger_records=psnger_records)
    flt_psnger_info_path = os.path.join(out_dir, "flt_psnger_info.csv")
    flt_psnger_info_title = ["flight_id", "passenger_number"]
    writeFile(flt_psnger_info, flt_psnger_info_path, title=flt_psnger_info_title)

    # calculate the nautical miles for each flight and the total miles for each passenger
    flt_dists, psnger_dists = calcFltDists(psnger_records=psnger_records, prt_records=prt_records)
    flt_dists_path = os.path.join(out_dir, "flt_dists.csv")
    flt_dists_title = ["flight_id", "flight_distance"]
    writeFile(flt_dists, flt_dists_path, title=flt_dists_title)

    psnger_dists_path = os.path.join(out_dir, "psnger_dists.csv")
    psnger_dists_title = ["passenger_id", "flight_distance"]
    writeFile(psnger_dists, psnger_dists_path, title=psnger_dists_title)

    print("Passenger '%s' has earned the highest air miles of '%s'!" %(psnger_dists[0][0], psnger_dists[0][1]))



main()