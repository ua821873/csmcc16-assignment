# CSMCC16 assignment

In order to complete the prototype, I separately implement each module. 

To make it clear, I put the different functions into several module files, including utils.py, mapreduce.py and main.py.

You can get all the results by running main.py in a Python 2.7 environment.