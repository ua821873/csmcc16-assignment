# encoding=utf-8

import time
from utils import distance, getDistance
import pdb

def calcFltNum(psnger_records, prt_records):
    """
    calculate the number of flights from each airport
    :param psnger_records: input passenger data, with format [psnger_id, flt_id, frm_code, dst_code, dpt_time, ttl_time]
    :param prt_records: input airport data
    :return:
    """
    # the map stage
    prt_flt = dict()
    for record in psnger_records:
        flt_id = record[1]
        frm_code = record[2]
        if frm_code in prt_flt:
            prt_flt[frm_code].add(flt_id)
        else:
            prt_flt[frm_code] = set([flt_id])
    # pdb.set_trace()
    # the reduce stage
    # calculate the number of flights  from each airport
    prt_flt_info = dict()
    for record in prt_records:
        prt_code = record[1]
        prt_flt_info[prt_code] = record + ["0"]
    for prt_code in prt_flt:
        flt_num = len(prt_flt[prt_code])
        if prt_code in prt_flt_info:
            prt_flt_info[prt_code][-1] = str(flt_num)
        else:
            prt_flt_info[prt_code] = ["", prt_code, "", "", str(flt_num)]

    prt_flt_info_out = prt_flt_info.values()
    prt_flt_info_out.sort()

    return prt_flt_info_out


def calcArvlTime(dpt_time, ttl_time):
    """
    calculate the arrival time, according to departure time and flight's total time
    :param dpt_time: the departure time in Unix format
    :param ttl_time: the flight's total time (minutes)
    :return: arv_time in string
    """
    arv_time = time.gmtime(int(dpt_time) + int(ttl_time))
    dpt_time = time.strftime("%H:%M:%S", time.gmtime(int(dpt_time)))
    arv_time = time.strftime("%H:%M:%S", arv_time)
    return dpt_time, arv_time


def calcFltInfo(psnger_records):
    """
        calculate the information of each flight
        :param psnger_records: input passenger data, with format [psnger_id, flt_id, frm_code, dst_code, dpt_time, ttl_time
        :return:    flt_info_out
        """
    # map procedure
    flt_info = dict()
    for record in psnger_records:
        psnger_id = record[0]
        flt_id = record[1]
        frm_code = record[2]
        dst_code = record[3]
        dpt_time = record[4]    # in Unix ‘epoch’ time
        ttl_time = record[5]    # mins
        dpt_time, arv_time = calcArvlTime(dpt_time, ttl_time)
        output_record = [flt_id, psnger_id, frm_code, dst_code, dpt_time, arv_time, ttl_time]
        if flt_id in flt_info:
            flt_info[flt_id].append(output_record)
        else:
            flt_info[flt_id] = [output_record]

    # reduce procedure
    flt_ids = flt_info.keys()
    flt_ids.sort()
    flt_info_out = []
    for flt_id in flt_ids:
        records = flt_info[flt_id]
        records.sort()
        flt_info_out.extend(records)

    return flt_info_out


def calcPsngNumber(psnger_records):
    """
        calculate the number of passengers on each flight
        :param psnger_records: input passenger data, with format [psnger_id, flt_id, frm_code, dst_code, dpt_time, ttl_time
        :return:    flt_info_out
        """
    # map procedure
    flt_info = dict()
    for record in psnger_records:
        psnger_id = record[0]
        flt_id = record[1]
        if flt_id in flt_info:
            flt_info[flt_id].append(psnger_id)
        else:
            flt_info[flt_id] = [psnger_id]

    # reduce procedure
    flt_ids = flt_info.keys()
    flt_ids.sort()
    flt_info_out = []
    for flt_id in flt_ids:
        psnger_num = len(flt_info[flt_id])
        flt_info_out.append([flt_id, str(psnger_num)])

    return flt_info_out


def calcFltDists(psnger_records, prt_records):
    """
    calculate the nautical miles for each flight and the total miles for each passenger
    :param psnger_records: input passenger data, with format [psnger_id, flt_id, frm_code, dst_code, dpt_time, ttl_time]
    :param prt_records: input airport data, with format [prt_name, prt_code, lat, lon]
    :return:
    """
    prt_flt = dict()
    prt_info = dict()
    for prt_record in prt_records:
        prt_code = prt_record[1]
        lat = float(prt_record[2])
        lon = float(prt_record[3])
        prt_info[prt_code] = [lat, lon]

    # the map procedure
    flt_dists = dict()
    psnger_dists = dict()
    for record in psnger_records:
        flt_id = record[1]
        frm_code = record[2]
        dst_code = record[3]
        if flt_id not in flt_dists:
            frm_lat, frm_lon = prt_info[frm_code]
            dst_lat, dst_lon = prt_info[dst_code]
            dist = distance(frm_lat, frm_lon, dst_lat, dst_lon)
            # dist2 = getDistance(frm_lat, frm_lon, dst_lat, dst_lon)
            # print(dist, dist2)
            flt_dists[flt_id] = dist

        psnger_id = record[0]
        if psnger_id not in psnger_dists:
            psnger_dists[psnger_id] = [dist]
        else:
            psnger_dists[psnger_id].append(dist)

    flt_dists_out = []
    flt_ids = flt_dists.keys()
    flt_ids.sort()
    for flt_id in flt_ids:
        flt_dists_out.append([flt_id, "%.3f" % flt_dists[flt_id]])

    # the reduce stage
    # calculate the total miles for each passenger
    psnger_dist_out = []
    for psnger_id in psnger_dists:
        total_dist = sum(psnger_dists[psnger_id])
        psnger_dist_out.append([psnger_id, "%.3f" % total_dist])

    psnger_dist_out.sort(key=lambda x: float(x[1]), reverse=True)
    # psnger_dist_out.sort(cmp=lambda x, y: cmp(float(x[1]), float(y[1])), reverse=True)

    return flt_dists_out, psnger_dist_out
